﻿namespace RWS_Test_App
{
    partial class TestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGetDocumentList = new System.Windows.Forms.Button();
            this.btnGetDocumentFile = new System.Windows.Forms.Button();
            this.btnUploadFile = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblRecordCount = new System.Windows.Forms.Label();
            this.dgvData = new System.Windows.Forms.DataGridView();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnDVGetFolderList = new System.Windows.Forms.Button();
            this.btnDVGetDocumentTypeList = new System.Windows.Forms.Button();
            this.btnGetDocFileByID = new System.Windows.Forms.Button();
            this.txtDownloadDocID = new System.Windows.Forms.TextBox();
            this.txtUploadFilePath = new System.Windows.Forms.TextBox();
            this.btnUploadFileByContent = new System.Windows.Forms.Button();
            this.btnLogin = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnFolderList = new System.Windows.Forms.Button();
            this.cmbFolderList = new System.Windows.Forms.ComboBox();
            this.btnDocumentCategories = new System.Windows.Forms.Button();
            this.cmbDocCategory = new System.Windows.Forms.ComboBox();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnMetaData = new System.Windows.Forms.Button();
            this.propGridMetaData = new System.Windows.Forms.PropertyGrid();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDocNo = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtUploadDocId = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtUniqueID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDeleteDocID = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtHostedURL = new System.Windows.Forms.TextBox();
            this.lblLoginStatus = new System.Windows.Forms.Label();
            this.lblToken = new System.Windows.Forms.Label();
            this.btnLogoff = new System.Windows.Forms.Button();
            this.btnDocLink = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.txtDocLinkDocID = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnGetDocumentList
            // 
            this.btnGetDocumentList.Location = new System.Drawing.Point(28, 37);
            this.btnGetDocumentList.Name = "btnGetDocumentList";
            this.btnGetDocumentList.Size = new System.Drawing.Size(148, 27);
            this.btnGetDocumentList.TabIndex = 0;
            this.btnGetDocumentList.Text = "Get-Document List";
            this.btnGetDocumentList.UseVisualStyleBackColor = true;
            this.btnGetDocumentList.Click += new System.EventHandler(this.btnGetDocumentList_Click);
            // 
            // btnGetDocumentFile
            // 
            this.btnGetDocumentFile.Location = new System.Drawing.Point(28, 103);
            this.btnGetDocumentFile.Name = "btnGetDocumentFile";
            this.btnGetDocumentFile.Size = new System.Drawing.Size(148, 27);
            this.btnGetDocumentFile.TabIndex = 1;
            this.btnGetDocumentFile.Text = "Get-Doc File by Unique ID";
            this.btnGetDocumentFile.UseVisualStyleBackColor = true;
            this.btnGetDocumentFile.Click += new System.EventHandler(this.btnGetDocumentFile_Click);
            // 
            // btnUploadFile
            // 
            this.btnUploadFile.Location = new System.Drawing.Point(28, 114);
            this.btnUploadFile.Name = "btnUploadFile";
            this.btnUploadFile.Size = new System.Drawing.Size(148, 27);
            this.btnUploadFile.TabIndex = 2;
            this.btnUploadFile.Text = "Upload File By Path";
            this.btnUploadFile.UseVisualStyleBackColor = true;
            this.btnUploadFile.Click += new System.EventHandler(this.btnUploadFile_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.lblRecordCount);
            this.panel1.Controls.Add(this.dgvData);
            this.panel1.Controls.Add(this.btnClear);
            this.panel1.Location = new System.Drawing.Point(525, 334);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(479, 318);
            this.panel1.TabIndex = 3;
            // 
            // lblRecordCount
            // 
            this.lblRecordCount.AutoSize = true;
            this.lblRecordCount.Location = new System.Drawing.Point(3, 291);
            this.lblRecordCount.Name = "lblRecordCount";
            this.lblRecordCount.Size = new System.Drawing.Size(85, 13);
            this.lblRecordCount.TabIndex = 27;
            this.lblRecordCount.Text = "Record Count: 0";
            // 
            // dgvData
            // 
            this.dgvData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvData.Location = new System.Drawing.Point(3, 2);
            this.dgvData.Name = "dgvData";
            this.dgvData.Size = new System.Drawing.Size(469, 276);
            this.dgvData.TabIndex = 0;
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(397, 284);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 27);
            this.btnClear.TabIndex = 26;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnDVGetFolderList
            // 
            this.btnDVGetFolderList.Location = new System.Drawing.Point(25, 29);
            this.btnDVGetFolderList.Name = "btnDVGetFolderList";
            this.btnDVGetFolderList.Size = new System.Drawing.Size(142, 27);
            this.btnDVGetFolderList.TabIndex = 5;
            this.btnDVGetFolderList.Text = "Get-Folder List";
            this.btnDVGetFolderList.UseVisualStyleBackColor = true;
            this.btnDVGetFolderList.Click += new System.EventHandler(this.btnDVGetFolderList_Click);
            // 
            // btnDVGetDocumentTypeList
            // 
            this.btnDVGetDocumentTypeList.Location = new System.Drawing.Point(25, 62);
            this.btnDVGetDocumentTypeList.Name = "btnDVGetDocumentTypeList";
            this.btnDVGetDocumentTypeList.Size = new System.Drawing.Size(142, 27);
            this.btnDVGetDocumentTypeList.TabIndex = 6;
            this.btnDVGetDocumentTypeList.Text = "Get - Document Type List";
            this.btnDVGetDocumentTypeList.UseVisualStyleBackColor = true;
            this.btnDVGetDocumentTypeList.Click += new System.EventHandler(this.btnDVGetDocumentTypeList_Click);
            // 
            // btnGetDocFileByID
            // 
            this.btnGetDocFileByID.Location = new System.Drawing.Point(28, 70);
            this.btnGetDocFileByID.Name = "btnGetDocFileByID";
            this.btnGetDocFileByID.Size = new System.Drawing.Size(148, 27);
            this.btnGetDocFileByID.TabIndex = 7;
            this.btnGetDocFileByID.Text = "Get-Doc File by ID";
            this.btnGetDocFileByID.UseVisualStyleBackColor = true;
            this.btnGetDocFileByID.Click += new System.EventHandler(this.btnGetDocFileByID_Click);
            // 
            // txtDownloadDocID
            // 
            this.txtDownloadDocID.Location = new System.Drawing.Point(254, 41);
            this.txtDownloadDocID.Name = "txtDownloadDocID";
            this.txtDownloadDocID.Size = new System.Drawing.Size(207, 20);
            this.txtDownloadDocID.TabIndex = 8;
            // 
            // txtUploadFilePath
            // 
            this.txtUploadFilePath.Location = new System.Drawing.Point(82, 36);
            this.txtUploadFilePath.Name = "txtUploadFilePath";
            this.txtUploadFilePath.Size = new System.Drawing.Size(381, 20);
            this.txtUploadFilePath.TabIndex = 9;
            // 
            // btnUploadFileByContent
            // 
            this.btnUploadFileByContent.Location = new System.Drawing.Point(28, 80);
            this.btnUploadFileByContent.Name = "btnUploadFileByContent";
            this.btnUploadFileByContent.Size = new System.Drawing.Size(148, 27);
            this.btnUploadFileByContent.TabIndex = 10;
            this.btnUploadFileByContent.Text = "Upload File Content ";
            this.btnUploadFileByContent.UseVisualStyleBackColor = true;
            this.btnUploadFileByContent.Click += new System.EventHandler(this.btnUploadFileByContent_Click);
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(218, 21);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(148, 27);
            this.btnLogin.TabIndex = 11;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(19, 27);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(148, 27);
            this.btnDelete.TabIndex = 12;
            this.btnDelete.Text = "Delete File";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnFolderList
            // 
            this.btnFolderList.Location = new System.Drawing.Point(28, 76);
            this.btnFolderList.Name = "btnFolderList";
            this.btnFolderList.Size = new System.Drawing.Size(148, 27);
            this.btnFolderList.TabIndex = 13;
            this.btnFolderList.Text = "List of Folders";
            this.btnFolderList.UseVisualStyleBackColor = true;
            this.btnFolderList.Click += new System.EventHandler(this.btnFolderList_Click);
            // 
            // cmbFolderList
            // 
            this.cmbFolderList.FormattingEnabled = true;
            this.cmbFolderList.Location = new System.Drawing.Point(28, 109);
            this.cmbFolderList.Name = "cmbFolderList";
            this.cmbFolderList.Size = new System.Drawing.Size(148, 21);
            this.cmbFolderList.TabIndex = 14;
            this.cmbFolderList.SelectedIndexChanged += new System.EventHandler(this.cmbFolderList_SelectedIndexChanged);
            // 
            // btnDocumentCategories
            // 
            this.btnDocumentCategories.Location = new System.Drawing.Point(28, 136);
            this.btnDocumentCategories.Name = "btnDocumentCategories";
            this.btnDocumentCategories.Size = new System.Drawing.Size(148, 27);
            this.btnDocumentCategories.TabIndex = 15;
            this.btnDocumentCategories.Text = "List of Doc Categories";
            this.btnDocumentCategories.UseVisualStyleBackColor = true;
            this.btnDocumentCategories.Click += new System.EventHandler(this.btnDocumentCategories_Click);
            // 
            // cmbDocCategory
            // 
            this.cmbDocCategory.FormattingEnabled = true;
            this.cmbDocCategory.Location = new System.Drawing.Point(28, 169);
            this.cmbDocCategory.Name = "cmbDocCategory";
            this.cmbDocCategory.Size = new System.Drawing.Size(148, 21);
            this.cmbDocCategory.TabIndex = 16;
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(28, 50);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(148, 20);
            this.txtUserName.TabIndex = 17;
            this.txtUserName.Text = "ADMIN";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "UserName";
            // 
            // btnMetaData
            // 
            this.btnMetaData.Location = new System.Drawing.Point(28, 196);
            this.btnMetaData.Name = "btnMetaData";
            this.btnMetaData.Size = new System.Drawing.Size(148, 27);
            this.btnMetaData.TabIndex = 19;
            this.btnMetaData.Text = "Get Meta Data Info";
            this.btnMetaData.UseVisualStyleBackColor = true;
            this.btnMetaData.Click += new System.EventHandler(this.btnMetaData_Click);
            // 
            // propGridMetaData
            // 
            this.propGridMetaData.LineColor = System.Drawing.SystemColors.ControlDark;
            this.propGridMetaData.Location = new System.Drawing.Point(3, 3);
            this.propGridMetaData.Name = "propGridMetaData";
            this.propGridMetaData.Size = new System.Drawing.Size(255, 137);
            this.propGridMetaData.TabIndex = 20;
            this.propGridMetaData.Click += new System.EventHandler(this.propGridMetaData_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.propGridMetaData);
            this.panel2.Location = new System.Drawing.Point(191, 76);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(265, 147);
            this.panel2.TabIndex = 21;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtDocNo);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.btnFolderList);
            this.groupBox1.Controls.Add(this.btnMetaData);
            this.groupBox1.Controls.Add(this.cmbFolderList);
            this.groupBox1.Controls.Add(this.btnDocumentCategories);
            this.groupBox1.Controls.Add(this.txtUserName);
            this.groupBox1.Controls.Add(this.cmbDocCategory);
            this.groupBox1.Location = new System.Drawing.Point(27, 63);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(479, 249);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Get eDMS Lists";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(188, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Document No";
            // 
            // txtDocNo
            // 
            this.txtDocNo.Location = new System.Drawing.Point(191, 50);
            this.txtDocNo.Name = "txtDocNo";
            this.txtDocNo.Size = new System.Drawing.Size(148, 20);
            this.txtDocNo.TabIndex = 22;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.txtUploadDocId);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.btnUploadFileByContent);
            this.groupBox2.Controls.Add(this.txtUploadFilePath);
            this.groupBox2.Controls.Add(this.btnUploadFile);
            this.groupBox2.Location = new System.Drawing.Point(27, 334);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(479, 156);
            this.groupBox2.TabIndex = 23;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Upload to eDMS";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(195, 87);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Doc ID:";
            // 
            // txtUploadDocId
            // 
            this.txtUploadDocId.Location = new System.Drawing.Point(256, 84);
            this.txtUploadDocId.Name = "txtUploadDocId";
            this.txtUploadDocId.Size = new System.Drawing.Size(207, 20);
            this.txtUploadDocId.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "File Path:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.txtUniqueID);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.btnGetDocFileByID);
            this.groupBox3.Controls.Add(this.btnGetDocumentFile);
            this.groupBox3.Controls.Add(this.txtDownloadDocID);
            this.groupBox3.Controls.Add(this.btnGetDocumentList);
            this.groupBox3.Location = new System.Drawing.Point(27, 508);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(479, 144);
            this.groupBox3.TabIndex = 24;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Download File from eDMS";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(188, 110);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Unique ID";
            // 
            // txtUniqueID
            // 
            this.txtUniqueID.Location = new System.Drawing.Point(249, 107);
            this.txtUniqueID.Name = "txtUniqueID";
            this.txtUniqueID.Size = new System.Drawing.Size(207, 20);
            this.txtUniqueID.TabIndex = 10;
            this.txtUniqueID.Text = "BE122620";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(188, 44);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Doc ID:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.txtDocLinkDocID);
            this.groupBox4.Controls.Add(this.btnDocLink);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.txtDeleteDocID);
            this.groupBox4.Controls.Add(this.btnDelete);
            this.groupBox4.Location = new System.Drawing.Point(525, 63);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(479, 130);
            this.groupBox4.TabIndex = 25;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "General Methods";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(185, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Doc ID:";
            // 
            // txtDeleteDocID
            // 
            this.txtDeleteDocID.Location = new System.Drawing.Point(235, 35);
            this.txtDeleteDocID.Name = "txtDeleteDocID";
            this.txtDeleteDocID.Size = new System.Drawing.Size(209, 20);
            this.txtDeleteDocID.TabIndex = 13;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btnDVGetFolderList);
            this.groupBox5.Controls.Add(this.btnDVGetDocumentTypeList);
            this.groupBox5.Location = new System.Drawing.Point(525, 199);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(479, 113);
            this.groupBox5.TabIndex = 27;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Test Methods";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(24, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 13);
            this.label7.TabIndex = 28;
            this.label7.Text = "Hosted URL:";
            // 
            // txtHostedURL
            // 
            this.txtHostedURL.Location = new System.Drawing.Point(27, 25);
            this.txtHostedURL.Name = "txtHostedURL";
            this.txtHostedURL.Size = new System.Drawing.Size(176, 20);
            this.txtHostedURL.TabIndex = 29;
            this.txtHostedURL.Text = "http://localhost:61418";
            // 
            // lblLoginStatus
            // 
            this.lblLoginStatus.AutoSize = true;
            this.lblLoginStatus.Location = new System.Drawing.Point(386, 21);
            this.lblLoginStatus.Name = "lblLoginStatus";
            this.lblLoginStatus.Size = new System.Drawing.Size(97, 13);
            this.lblLoginStatus.TabIndex = 30;
            this.lblLoginStatus.Text = "Login Status: False";
            this.lblLoginStatus.Click += new System.EventHandler(this.label8_Click);
            // 
            // lblToken
            // 
            this.lblToken.AutoSize = true;
            this.lblToken.Location = new System.Drawing.Point(386, 35);
            this.lblToken.Name = "lblToken";
            this.lblToken.Size = new System.Drawing.Size(44, 13);
            this.lblToken.TabIndex = 31;
            this.lblToken.Text = "Token: ";
            // 
            // btnLogoff
            // 
            this.btnLogoff.Location = new System.Drawing.Point(884, 21);
            this.btnLogoff.Name = "btnLogoff";
            this.btnLogoff.Size = new System.Drawing.Size(115, 27);
            this.btnLogoff.TabIndex = 32;
            this.btnLogoff.Text = "Logoff";
            this.btnLogoff.UseVisualStyleBackColor = true;
            this.btnLogoff.Click += new System.EventHandler(this.btnLogoff_Click);
            // 
            // btnDocLink
            // 
            this.btnDocLink.Location = new System.Drawing.Point(19, 60);
            this.btnDocLink.Name = "btnDocLink";
            this.btnDocLink.Size = new System.Drawing.Size(148, 27);
            this.btnDocLink.TabIndex = 7;
            this.btnDocLink.Text = "Document Link";
            this.btnDocLink.UseVisualStyleBackColor = true;
            this.btnDocLink.Click += new System.EventHandler(this.btnDocLink_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(185, 67);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Doc ID:";
            // 
            // txtDocLinkDocID
            // 
            this.txtDocLinkDocID.Location = new System.Drawing.Point(235, 64);
            this.txtDocLinkDocID.Name = "txtDocLinkDocID";
            this.txtDocLinkDocID.Size = new System.Drawing.Size(209, 20);
            this.txtDocLinkDocID.TabIndex = 15;
            // 
            // TestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1019, 666);
            this.Controls.Add(this.btnLogoff);
            this.Controls.Add(this.lblToken);
            this.Controls.Add(this.lblLoginStatus);
            this.Controls.Add(this.txtHostedURL);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "TestForm";
            this.Text = "RWS Test App";
            this.Load += new System.EventHandler(this.TestForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.panel2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGetDocumentList;
        private System.Windows.Forms.Button btnGetDocumentFile;
        private System.Windows.Forms.Button btnUploadFile;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.Button btnDVGetFolderList;
        private System.Windows.Forms.Button btnDVGetDocumentTypeList;
        private System.Windows.Forms.Button btnGetDocFileByID;
        private System.Windows.Forms.TextBox txtDownloadDocID;
        private System.Windows.Forms.TextBox txtUploadFilePath;
        private System.Windows.Forms.Button btnUploadFileByContent;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnFolderList;
        private System.Windows.Forms.ComboBox cmbFolderList;
        private System.Windows.Forms.Button btnDocumentCategories;
        private System.Windows.Forms.ComboBox cmbDocCategory;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnMetaData;
        private System.Windows.Forms.PropertyGrid propGridMetaData;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDeleteDocID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDocNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtUniqueID;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtHostedURL;
        private System.Windows.Forms.Label lblRecordCount;
        private System.Windows.Forms.Label lblLoginStatus;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtUploadDocId;
        private System.Windows.Forms.Label lblToken;
        private System.Windows.Forms.Button btnLogoff;
        private System.Windows.Forms.Button btnDocLink;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtDocLinkDocID;
    }
}

