﻿using Newtonsoft.Json;
using RWS_Test_App.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace RWS_Test_App
{
    public partial class TestForm : Form
    {
        private string FolderName = string.Empty;
        MetaDataInfoProperties metaDataList = new MetaDataInfoProperties();
        internal string token = string.Empty;

        public TestForm()
        {
            InitializeComponent();
        }

        private async void btnGetDocumentList_Click(object sender, EventArgs e)
        {
            try
            {
                string URL = string.Format("{0}/DocuVision.svc/dvGetDocumentList", txtHostedURL.Text);

                if (cmbFolderList.SelectedItem == null)
                {
                    MessageBox.Show("Please ensure a eDMS Folder and Document No is captured!", "Please verify data!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                RequestDocumetList getDocList = new RequestDocumetList
                {
                    FolderName = cmbFolderList.SelectedItem.ToString(),
                    DocumentID = string.IsNullOrEmpty(txtDownloadDocID.Text) ? "" : txtDownloadDocID.Text,
                    DocumentNo = txtDocNo.Text.Trim(),
                    DocumentCategory = cmbDocCategory.SelectedItem == null ? "" : cmbDocCategory.SelectedItem.ToString(),
                    //SearchID = "BE122620"
                    SearchID = "",
                    Token = token
                };


                //POST
                using (var client = new HttpClient())
                {
                    string jsonContent = JsonConvert.SerializeObject(getDocList);
                    var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");
                    var Result = client.PostAsync(URL, content).Result;

                    string value = Result.ReasonPhrase;
                    string jsonResult = await Result.Content.ReadAsStringAsync();

                    var result = JsonConvert.DeserializeObject<Response>(jsonResult);
                    var list = JsonConvert.DeserializeObject<List<DocumentListResults>>(result.Result);

                    if (result.ResponseStatus)
                    {

                        dgvData.Columns.Clear();
                        dgvData.Rows.Clear();

                        dgvData.Columns.Add("DocNo", "DocNo");
                        dgvData.Columns.Add("DocID", "DocID");
                        dgvData.Columns.Add("DocumentCategory", "DocumentCategory");
                        dgvData.Columns.Add("UpdateDate", "UpdateDate");

                        int count = 0;
                        foreach (var item in list)
                        {
                            count++;
                            dgvData.Rows.Add(item.DocumentNo, item.DocumentID, item.DocumentCategory, item.UpdatedDate);
                        }
                        lblRecordCount.Text = string.Format("Record count: {0}", count.ToString()); 
                    }
                    else
                    {
                        MessageBox.Show(result.ErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                //throw ex;
                MessageBox.Show(ex.Message);
            }
        }

        private async void btnGetDocumentFile_Click(object sender, EventArgs e)
        {
            try
            {
                string URL = string.Format("{0}/DocuVision.svc/dVDownloadDocumentFileByUniqueID", txtHostedURL.Text);
                //string URL = "http://localhost:61418/DocuVision.svc/dvGetDocumentFileByUniqueID";

                if (string.IsNullOrEmpty(txtDocNo.Text))
                {
                    MessageBox.Show("Please enter a Document ID.", "Missing Document ID", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                if (cmbFolderList.SelectedItem == null)
                {
                    MessageBox.Show("Please enter a eDMS Folder is Selected.", "Please Select eDMS Folder", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                RequestDocumentFileByUniqueID getDocFile = new RequestDocumentFileByUniqueID
                {
                    FolderName = cmbFolderList.SelectedItem.ToString(),
                    DocumentNo = txtDocNo.Text.Trim(),
                    UniqueID = txtUniqueID.Text.Trim(),
                    Token = token
                };


                using (var client = new HttpClient())
                {
                    //POST
                    string jsonContent = JsonConvert.SerializeObject(getDocFile);
                    var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");
                    var Result = client.PostAsync(URL, content).Result;

                    string value = Result.ReasonPhrase;
                    string jsonResult = await Result.Content.ReadAsStringAsync();

                    var result = JsonConvert.DeserializeObject<Response>(jsonResult);
                    var file = JsonConvert.DeserializeObject<GetDocumentFile>(result.Result);
                }
            }
            catch (Exception ex)
            {
                //throw ex;
                MessageBox.Show(ex.Message);
            }
        }

        private async void btnDVGetFolderList_Click(object sender, EventArgs e)
        {
            string URL = string.Format("{0}/DocuVision.svc/dvGetFolderList/Purchasing", txtHostedURL.Text);
            //string URL = "http://localhost:61418/DocuVision.svc/dvGetFolderList/Purchasing";
            try
            {
                using (var client = new HttpClient())
                {
                    var Result = client.GetAsync(URL).Result;
                    string jsonResult = await Result.Content.ReadAsStringAsync();

                    string stringList = JsonConvert.DeserializeObject<string>(jsonResult);
                    var documentFolderList = JsonConvert.DeserializeObject<Dictionary<string, string>>(stringList);

                    dgvData.Columns.Clear();
                    dgvData.Rows.Clear();

                    dgvData.Columns.Add("Key", "Key");
                    dgvData.Columns.Add("Value", "Value");

                    int count = 0;
                    foreach (var item in documentFolderList)
                    {
                        dgvData.Rows.Add(item.Key, item.Value);
                        count++;
                    }
                    lblRecordCount.Text = string.Format("Record count: {0}", count.ToString());
                }
            }
            catch (Exception ex)
            {
                //throw ex;
                MessageBox.Show(ex.Message);
            }
        }

        private async void btnDVGetDocumentTypeList_Click(object sender, EventArgs e)
        {
            string URL = string.Format("{0}/DocuVision.svc/dvGetDocumentTypeList", txtHostedURL.Text);
            //string URL = "http://localhost:61418/DocuVision.svc/dvGetDocumentTypeList";

            try
            {
                using (var client = new HttpClient())
                {
                    var Result = client.GetAsync(URL).Result;
                    string jsonResult = await Result.Content.ReadAsStringAsync();

                    string stringList = JsonConvert.DeserializeObject<string>(jsonResult);
                    var documentTypeList = JsonConvert.DeserializeObject<Dictionary<string, string>>(stringList);

                    dgvData.Columns.Clear();
                    dgvData.Rows.Clear();

                    dgvData.Columns.Add("Key", "Key");
                    dgvData.Columns.Add("Value", "Value");

                    int count = 0;
                    foreach (var item in documentTypeList)
                    {
                        dgvData.Rows.Add(item.Key, item.Value);
                        count++;
                    }

                    lblRecordCount.Text = string.Format("Record count: {0}", count.ToString());
                }
            }
            catch (Exception ex)
            {
                //throw ex;
                MessageBox.Show(ex.Message);
            }
        }

        private async void btnGetDocFileByID_Click(object sender, EventArgs e)
        {
            string URL = string.Format("{0}/DocuVision.svc/dVDownloadDocumentFileByDocID", txtHostedURL.Text);
            //string URL = "http://localhost:61418/DocuVision.svc/dVDownloadDocumentFileByDocID";

            string docId = txtDownloadDocID.Text;

            if (string.IsNullOrEmpty(docId))
            {
                MessageBox.Show("Please enter a Document ID.", "Missing Document ID", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (cmbFolderList.SelectedItem == null)
            {
                MessageBox.Show("Please enter a eDMS Folder is Selected.", "Please Select eDMS Folder", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {
                try
                {
                    RequestDocumentByID getDocbyID = new RequestDocumentByID
                    {
                        FolderName = cmbFolderList.SelectedItem.ToString(),
                        DocumentNo = txtDocNo.Text.Trim(),
                        DocumentID = txtDownloadDocID.Text.Trim(),
                        Token = token
                    };


                    //POST
                    using (var client = new HttpClient())
                    {
                        string jsonContent = JsonConvert.SerializeObject(getDocbyID);
                        var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");
                        var Result = client.PostAsync(URL, content).Result;

                        string value = Result.ReasonPhrase;
                        string jsonResult = await Result.Content.ReadAsStringAsync();

                        var result = JsonConvert.DeserializeObject<Response>(jsonResult);
                        var list = JsonConvert.DeserializeObject<GetDocumentFile>(result.Result);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    //throw ex;
                }
            }
        }

        private async void btnUploadFile_Click(object sender, EventArgs e)
        {
            XmlDocument doc = new XmlDocument();
            XmlElement root = doc.CreateElement(FolderName.Replace(" ", "").Trim());
            //TODO Build XML string from Meta Data Properties
            foreach (var item in metaDataList.MetaDataProperties)
            {
                XmlElement elem = doc.CreateElement(item.ColumnName ?? "");
                elem.SetAttribute("DataType", item.DataType ?? "");
                elem.SetAttribute("MaxLength", item.MaxLength ?? "");
                elem.InnerText = item.Value;

                root.AppendChild(elem);
                doc.AppendChild(root);
            }
            string xmltest = doc.OuterXml;

            //FileInfo fileInfo = new FileInfo(@"C: \Users\svdmerwe\Desktop\Sample Documents\Web Services\WebTest1.PDF");
            //string filePath = fileInfo.FullName;
            UploadDocumnetFileByPath uploadFile = new UploadDocumnetFileByPath
            {
                DocumentNo = txtDocNo.Text,
                FolderName = cmbFolderList.SelectedItem.ToString(),
                DocumentCategory = cmbDocCategory.SelectedItem.ToString(),
                FullFileName = txtUploadFilePath.Text,
                FileExtension = "PDF",
                MetaDataXml = doc.OuterXml,
                Token = token
            };

            string URL = string.Format("{0}/DocuVision.svc/dvUploadDocumentFileByPath", txtHostedURL.Text);
            //string URL = "http://localhost:61418/DocuVision.svc/dvUploadDocumentFileByPath";

            try
            {
                using (var client = new HttpClient())
                {
                    string jsonContent = JsonConvert.SerializeObject(uploadFile);
                    var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");
                    var Result = client.PostAsync(URL, content).Result;

                    string value = Result.ReasonPhrase;
                    string jsonResult = await Result.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<Response>(jsonResult);
                    var uploadResutl = result.Result;
                }
            }
            catch (Exception ex)
            {
                //throw ex;
                MessageBox.Show(ex.Message);
            }
        }

        private async void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                string URL = string.Format("{0}/DocuVision.svc/dvLogin", txtHostedURL.Text);
                //string URL = "http://localhost:61418/DocuVision.svc/dvLogin/";

                Login loginInfo = new Login()
                {
                    UserName = "ADMIN",
                    Password = "Adm@Pwd2",
                    Project = "EDMS"
                };

                using (var client = new HttpClient())
                {
                    string jsonContent = JsonConvert.SerializeObject(loginInfo);
                    var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");
                    var Result = client.PostAsync(URL, content).Result;

                    string value = Result.ReasonPhrase;
                    string jsonResult = await Result.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<Response>(jsonResult);
                    var uploadResutl = result.Result;
                    token = result.Result;
                    if (!String.IsNullOrEmpty(token))
                    {
                        lblLoginStatus.Text = "Login Status: True";
                        lblToken.Text = token;
                    }
                }
            }
            catch (Exception ex)
            {
                //throw ex;
                MessageBox.Show(ex.Message);
            }
        }

        private async void btnDelete_Click(object sender, EventArgs e)
        {
            //string DocID = txtDeleteDocID.Text.Trim();
            DeleteDocumentFile deleteFile = new DeleteDocumentFile()
            {
                DocumentID = txtDeleteDocID.Text.Trim(),
                Token = token
            };

            string URL = string.Format("{0}/DocuVision.svc/dvDeleteDocument", txtHostedURL.Text);
            //string URL = "http://localhost:61418/DocuVision.svc/dvDeleteDocument/";

            try
            {
                using (var client = new HttpClient())
                {
                    string jsonContent = JsonConvert.SerializeObject(deleteFile);
                    var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");
                    var Result = client.PostAsync(URL, content).Result;

                    string value = Result.ReasonPhrase;
                    string jsonResult = await Result.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<Response>(jsonResult);
                    var deleteResult = result.Result;
                }
            }
            catch (Exception ex)
            {
                //throw ex;
                MessageBox.Show(ex.Message);
            }
        }

        #region Upload with Meta Data
        private async void btnFolderList_Click(object sender, EventArgs e)
        {
            string URL = string.Format("{0}/DocuVision.svc/dvGetFolders", txtHostedURL.Text);

            //string URL = "http://localhost:61418/DocuVision.svc/dvGetFolders";
            try
            {
                cmbFolderList.Items.Clear();
                using (var client = new HttpClient())
                {                  
                    string jsonContent = JsonConvert.SerializeObject(token);
                    var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");
                    var Result = client.PostAsync(URL, content).Result;

                    string value = Result.ReasonPhrase;
                    string jsonResult = await Result.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<Response>(jsonResult);
                    if (result.ErrorCode > 0)
                    {
                        MessageBox.Show(result.ErrorMessage);
                    }
                    else
                    {
                        List<string> folderList = JsonConvert.DeserializeObject<List<string>>(result.Result);

                        //TODO - Handle failed requests

                        foreach (var item in folderList)
                        {
                            cmbFolderList.Items.Add(item);
                        }
                        cmbFolderList.SelectedItem = cmbFolderList.Items[0];
                    }                    
                }
            }
            catch (Exception ex)
            {
                //throw ex;
                MessageBox.Show(ex.Message);
            }
        }

        private async void btnDocumentCategories_Click(object sender, EventArgs e)
        {
            string URL = string.Format("{0}/DocuVision.svc/dvGetDocumentCategories", txtHostedURL.Text);
            //string URL = "http://localhost:61418/DocuVision.svc/dvGetDocumentCategories/";

            try
            {
                RequestDocumentCategories request = new RequestDocumentCategories()
                {
                    Token = token,
                    FolderName = this.FolderName
                };

                cmbDocCategory.Items.Clear();
                using (var client = new HttpClient())
                {
                    string jsonContent = JsonConvert.SerializeObject(request);
                    var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");
                    var Result = client.PostAsync(URL, content).Result;

                    string value = Result.ReasonPhrase;
                    string jsonResult = await Result.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<Response>(jsonResult);
                    List<string> folderList = JsonConvert.DeserializeObject<List<string>>(result.Result);

                    foreach (var item in folderList)
                    {
                        cmbDocCategory.Items.Add(item);
                    }
                    cmbDocCategory.SelectedItem = cmbDocCategory.Items[0];
                }
            }
            catch (Exception ex)
            {
                //throw ex;
                MessageBox.Show(ex.Message);
            }
        }

        private async void btnMetaData_Click(object sender, EventArgs e)
        {
            string URL = string.Format("{0}/DocuVision.svc/dvGetMetaDataInfo", txtHostedURL.Text);
            //string URL = "http://localhost:61418/DocuVision.svc/dvGetMetaDataInfo/";

            try
            {
                RequestMetaDataInfo request = new RequestMetaDataInfo()
                {
                    Token = token,
                    //FolderName = FolderName.Replace(" ", "").Trim()
                    FolderName = FolderName
                };

                using (var client = new HttpClient())
                {
                    string jsonContent = JsonConvert.SerializeObject(request);
                    var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");
                    var Result = client.PostAsync(URL, content).Result;

                    string value = Result.ReasonPhrase;
                    string jsonResult = await Result.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<Response>(jsonResult);
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(result.Result);
                    XmlNodeList metaDataXmlList = xmlDoc.SelectNodes(string.Format("//{0}", FolderName.Replace(" ", "").Trim()));
                    XmlNode metaDataXml = metaDataXmlList[0];


                    MetaDataProperty metaDataProps = new MetaDataProperty();
                    foreach (XmlNode item in metaDataXml.ChildNodes)
                    {
                        metaDataList.MetaDataProperties.Add(new MetaDataProperty()
                        {
                            ColumnName = item.Name,
                            DataType = item.Attributes[0].InnerText,
                            MaxLength = item.Attributes[1].InnerText,
                            Value = item.Name == "DocumentCategory" ? cmbDocCategory.SelectedItem.ToString() : ""
                        });
                    }
                    //int i = x < 7 ? x : 7;
                    //cmbDocCategory.SelectedItem = cmbDocCategory.Items[0];
                    propGridMetaData.SelectedObject = metaDataList;
                }
            }
            catch (Exception ex)
            {
                //throw ex;
                MessageBox.Show(ex.Message);
            }
        }

        private async void btnUploadFileByContent_Click(object sender, EventArgs e)
        {
            try
            {
                Byte[] bytes = null;
                string base64File = string.Empty;
                string extension = string.Empty;
                string fileName = string.Empty;

                if (txtUploadDocId.Text == "")
                {
                     bytes = File.ReadAllBytes(txtUploadFilePath.Text);
                     base64File = Convert.ToBase64String(bytes);
                     FileInfo file = new FileInfo(txtUploadFilePath.Text);
                     extension = file.Extension;
                    fileName = file.Name;
                }                

                XmlDocument doc = new XmlDocument();
                XmlElement root = doc.CreateElement(FolderName.Replace(" ", "").Trim());
                //TODO Build XML string from Meta Data Properties
                foreach (var item in metaDataList.MetaDataProperties)
                {
                    XmlElement elem = doc.CreateElement(item.ColumnName ?? "");
                    elem.SetAttribute("DataType", item.DataType ?? "");
                    elem.SetAttribute("MaxLength", item.MaxLength ?? "");
                    elem.InnerText = item.Value;
                    //XmlText text = doc.CreateTextNode(item.Value);

                    root.AppendChild(elem);
                    doc.AppendChild(root);
                }
                string xmltest = doc.OuterXml;

                UploadDocumentFileByContent uploadFileByContent = new UploadDocumentFileByContent()
                {
                    //DocNo = txtDocNo.Text,
                    DocumentID = txtUploadDocId.Text.Trim(),
                    FolderName = cmbFolderList.Text = cmbFolderList.Text == "" ? "" : cmbFolderList.Text,
                    //EdmsFolderName = "",
                    //EdmsDocumentCategory = cmbDocCategory.SelectedItem.ToString(),
                    //EdmsDocumentCategory = "",
                    FileName = fileName,
                    FileExtension = extension,
                    FileContent = base64File,
                    MetaDataXml = doc.OuterXml,
                    Token = token
                };

                string URL = string.Format("{0}/DocuVision.svc/dvUploadDocumentFileByContent", txtHostedURL.Text);
                //string URL = "http://localhost:61418/DocuVision.svc/dvUploadDocumentFileByContent/";

                using (var client = new HttpClient())
                {
                    string jsonContent = JsonConvert.SerializeObject(uploadFileByContent);
                    var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");
                    var Result = client.PostAsync(URL, content).Result;

                    string value = Result.ReasonPhrase;
                    string jsonResult = await Result.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<Response>(jsonResult);
                    var uploadResutl = result.Result;
                }
            }
            catch (Exception ex)
            {
                //throw ex;
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        private void cmbFolderList_SelectedIndexChanged(object sender, EventArgs e)
        {
            FolderName = cmbFolderList.SelectedItem.ToString();
        }

        private void TestForm_Load(object sender, EventArgs e)
        {
            //FolderName retrieved from Get Folder Web Method
            //Category retrieved from Get Document Categories
        }

        private void propGridMetaData_Click(object sender, EventArgs e)
        {

        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            dgvData.Columns.Clear();
            dgvData.Rows.Clear();
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private async void btnLogoff_Click(object sender, EventArgs e)
        {
            try
            {
                string URL = string.Format("{0}/DocuVision.svc/dvLogoff", txtHostedURL.Text);
                //string URL = "http://localhost:61418/DocuVision.svc/dvLogin/";


                using (var client = new HttpClient())
                {
                    string jsonContent = JsonConvert.SerializeObject(lblToken.Text.Trim());
                    var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");
                    var Result = client.PostAsync(URL, content).Result;

                    string value = Result.ReasonPhrase;
                    string jsonResult = await Result.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<Response>(jsonResult);
                    var uploadResutl = result.Result;
                    token = result.Result;
                    if (!String.IsNullOrEmpty(token))
                    {
                        lblLoginStatus.Text = "Login Status: True";
                    }
                }
            }
            catch (Exception ex)
            {
                //throw ex;
                MessageBox.Show(ex.Message);
            }
        }

        private async void btnDocLink_Click(object sender, EventArgs e)
        {
            //string DocID = txtDeleteDocID.Text.Trim();
            DocumentLink docLink = new DocumentLink()
            {
                DocumentID = txtDocLinkDocID.Text.Trim(),
                Token = token
            };

            string URL = string.Format("{0}/DocuVision.svc/dvDocumentLink", txtHostedURL.Text);

            try
            {
                using (var client = new HttpClient())
                {
                    string jsonContent = JsonConvert.SerializeObject(docLink);
                    var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");
                    var Result = client.PostAsync(URL, content).Result;

                    string value = Result.ReasonPhrase;
                    string jsonResult = await Result.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<Response>(jsonResult);
                    var docLinkresult = result.Result;
                }
            }
            catch (Exception ex)
            {
                //throw ex;
                MessageBox.Show(ex.Message);
            }
        }
    }
}