﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RWS_Test_App.Models
{
    public class DeleteDocumentFile
    {
        public string DocumentID { get; set; }
        public string Token { get; set; }
    }
}
