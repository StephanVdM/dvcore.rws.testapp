﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RWS_Test_App.Models
{
    class UploadDocumentFileByContent
    {
        public string DocumentNo { get; set; }
        public string DocumentID { get; set; }
        public string FileContent { get; set; }
        public string FolderName { get; set; }
        public string FileName { get; set; }
        public string FileExtension { get; set; }
        public string MetaDataXml { get; set; }
        public string Token { get; set; }
    }
}
