﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RWS_Test_App.Models
{
    public class RequestDocumentByID
    {
        //public string DocType { get; set; }
        public string FolderName { get; set; }
        public string DocumentNo { get; set; }
        public string DocumentID { get; set; }
        public string Token { get; set; }
    }
}
