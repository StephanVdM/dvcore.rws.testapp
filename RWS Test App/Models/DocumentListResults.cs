﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RWS_Test_App.Models
{
    public class DocumentListResults
    {
        public string DocumentID { get; set; }
        public string DocumentNo { get; set; }
        public string DocumentCategory { get; set; }
        public string UpdatedDate { get; set; }
        public List<MetaData> MetaData { get; set; }


        //public string DocumentID { get; set; }
        //public string DocumentNo { get; set; }
        //public string SiteNo { get; set; }
        //public string BuildingName { get; set; }
        //public string CostCentre { get; set; }
        //public string Portfolio { get; set; }
        //public string RevenueType { get; set; }
        //public string FeeBased { get; set; }
        //public string Munic { get; set; }
        //public string Bulk { get; set; }
        //public string ContractNo { get; set; }
        //public string CustomerNo { get; set; }
        //public string CustomerName { get; set; }
        //public string UnitOwner { get; set; }
        //public string ElectricMeterNo { get; set; }
        //public string WaterMeterNo { get; set; }
        //public string GasMeterNo { get; set; }
        //public string SolarMeterNo { get; set; }
        //public string VirtualMeterNo { get; set; }
        //public string UtilityBillNo { get; set; }
        //public string PECCheckerMeter { get; set; }
        //public string ReadingCycle { get; set; }
        //public string ReadingDate { get; set; }
        //public string MeterReadingID { get; set; }
        //public string DocumentCategory { get; set; }
        //public string TicketNo { get; set; }
        //public string UserUpdated { get; set; }
        //public string UpdatedDate { get; set; }
    }
    public class MetaData
    {
        public string Field { get; set; }
        public string Data { get; set; }
        public string Type { get; set; }
        public string Length { get; set; }
        public string Mode { get; set; }
    }
}
