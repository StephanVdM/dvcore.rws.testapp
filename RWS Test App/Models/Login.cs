﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RWS_Test_App.Models
{
    class Login
    {
            public string UserName { get; set; }
            public string Password { get; set; }
            public string Project { get; set; }
    }
}
