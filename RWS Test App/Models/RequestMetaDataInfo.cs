﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RWS_Test_App.Models
{
    class RequestMetaDataInfo
    {
        public string Token { get; set; }
        public string FolderName { get; set; }
    }
}
