﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RWS_Test_App.Models
{
    public class UploadDocumnetFileByPath
    {
        public string DocumentNo { get; set; }
        public string FullFileName { get; set; }
        public string FolderName { get; set; }
        public string DocumentCategory { get; set; }
        public string FileExtension { get; set; }
        public string MetaDataXml { get; set; }
        public string Token { get; set; }
    }
}
