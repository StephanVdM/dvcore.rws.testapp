﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RWS_Test_App.Models
{
    class MetaDataInfoProperties
    {
        private MetaDataProperties metaDataProperties = new MetaDataProperties();
        /// <summary>
        /// gets or sets Message templates
        /// </summary>
        /// <returns></returns>
        [DisplayName("Meta Data Properties"), Category("Meta Data Properties")]
        [Description("Meta Data Properties")]
        //public List<Template> Templates
        public MetaDataProperties MetaDataProperties
        {
            get { return metaDataProperties; }
            set { if (value != metaDataProperties) metaDataProperties = value; }
        }
    }
    class MetaDataProperties : List<MetaDataProperty>
    {
        public override string ToString()
        {
            return string.Format("[{0} Item{1}]", this.Count, (this.Count == 1) ? "" : "s");
        }
    }

    class MetaDataProperty
    {
        public string ColumnName { get; set; }
        public string DataType { get; set; }
        public string MaxLength { get; set; }
        public string Value { get; set; }

        public override string ToString()
        {
            return string.Format("{0}", this.ColumnName);
        }
    }
}
