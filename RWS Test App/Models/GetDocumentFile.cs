﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RWS_Test_App.Models
{
    class GetDocumentFile
    {
        public string FileName { get; set; }
        public string FileData { get; set; }
        public List<MetaData> MetaData { get; set; }
    }
}
